/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 * Clase de prueba para el manejo de un caso problémico
 *
 * @author madar
 */
public class Prueba_Generico {

    public static void main(String[] args) {
        String nombres[] = {"andres", "jose", "yulieth", "deisler"};
        Integer codigos[] = {1152030, 1152031, 1152032, 1152033};
        Float notas[] = {3.5F, 5F, 4.3F, 5F};

        //Caso problémico: :(
//        for(String dato: nombres)
//            System.out.print(dato+"\t");
//        System.out.println("");
//        
//        for(int dato: codigos)
//            System.out.print(dato+"\t");
//        System.out.println("");
//        
//        
//        for(float dato: notas)
//            System.out.print(dato+"\t");
//        System.out.println("");
        imprimirVector(nombres);
        imprimirVector(codigos);
        imprimirVector(notas);

    }

    // Solución genérica:
    public static <T> void imprimirVector(T vector[]) {

        for (T dato : vector) {
            System.out.print(dato + "\t");
        }
        System.out.println("");

    }

}
