/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Jose Florez
 */
public class Fraccion implements Comparable {

    private int numerador, denominador;

    public Fraccion() {
    }

    public Fraccion(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    @Override
    public String toString() {
        return "Fraccion{" + "" + numerador + "/" + denominador + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.numerador;
        hash = 59 * hash + this.denominador;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fraccion other = (Fraccion) obj;
        if (this.numerador != other.numerador) {
            return false;
        }
        if (this.denominador != other.denominador) {
            return false;
        }
        //suposición faltaría realizar la  división... Ej: 1/2 == 2/4
        // Solución:
        if (this.numerador / this.denominador != other.numerador / other.denominador) {
            return false;
        }

        return true;
    }

    /**
     * Compara valores fraccionarios
     *
     * @param o objeto fracccionario
     * @return 0 si son iguales, -1 si this < de o y 1 si this > o
     */
    @Override
    public int compareTo(Object o) {
        if (this.equals(o)) {
            return 0;
        }
        final Fraccion other = (Fraccion) o;
        if (this.numerador / this.denominador > other.numerador / other.denominador) {
            return 1;
        }
        return -1;
    }

}
