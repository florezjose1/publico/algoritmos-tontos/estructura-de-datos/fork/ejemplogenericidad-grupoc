/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase genérica para el manejo de operaciones sobre vectores
 *
 * @author Jose Florez
 */
public class ManejoVector<T> {

    private T[] vector;

    public ManejoVector() {
    }

    public ManejoVector(T[] vector) {
        this.vector = vector;
    }

    public T[] getVector() {
        return vector;
    }

    public void setVector(T[] vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {

        String msg = "\t";
        for (T dato : vector) {
            msg += dato + "\t";
        }
        msg += "\n";

        return msg;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        //Sean del mismo tipo
        if (getClass() != obj.getClass()) {
            return false;
        }

        final ManejoVector<T> other = (ManejoVector<T>) obj;

        if (this.vector.length != other.vector.length) {
            return false;
        }

        for (int i = 0; i < this.vector.length; i++) {
            if (!this.vector[i].equals(other.vector[i])) {
                return false;
            }

        }

        return true;

    }

    /**
     * Ordena el vector en sí mismo, usando el método de selección,
     * El ordenamiento por selección consta de buscar el menor y colocarlo en la primera posición,
     * entre los restantes se busca el más pequeño y se coloca en la posición consecutiva hasta terminar
     * https://runestone.academy/runestone/static/pythoned/SortSearch/ElOrdenamientoPorSeleccion.html#:~:text=El%20ordenamiento%20por%20selecci%C3%B3n%20mejora,pone%20en%20la%20ubicaci%C3%B3n%20correcta.
     * 
     * Ordenamiento por selección tomado como ejemplo: http://puntocomnoesunlenguaje.blogspot.com/2012/12/java-metodo-ordenacion-seleccion.html
     * Recomendaciones: - Implementar compareTo en la clase Fraccion
     */
    public void ordenarPorSeleccion() {
        int p; // una variable para tomar la posición del elemento
        T temp, menor; // Dos elementos, uno temporal para el cambio y uno para almacenar el variante
        for (int i = 0; i < this.vector.length; i++) {
            menor = this.vector[i];
            p = i;
            
            for(int j = i+1; j<this.vector.length; j++) {
                
                int c = ((Comparable)this.vector[j]).compareTo(menor);
                if(c < 0) {
                    menor = this.vector[j];
                    p = j;
                }
                
            }
            if(p != i) {
                temp = this.vector[i];
                this.vector[i] = this.vector[p];
                this.vector[p] = temp;
            }
        }
    }

}
